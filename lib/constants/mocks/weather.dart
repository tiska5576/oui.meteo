// ignore_for_file: non_constant_identifier_names

import 'package:oui_sncf_weather/constants/config/routes.dart';

Map<String, dynamic> MockedWeather = Map
    .fromEntries(Routes.Weather.keys.map((city) {

  return MapEntry(city, {
    "cod": "200",
    "message": 0,
    "cnt": 40,
    "list": [
      {
        "dt": 1613779200,
        "main": {
          "temp": 10.32,
          "feels_like": 6.83,
          "temp_min": 10.32,
          "temp_max": 10.4,
          "pressure": 1013,
          "sea_level": 1013,
          "grnd_level": 1008,
          "humidity": 81,
          "temp_kf": -0.08
        },
        "weather": [
          {
            "id": 802,
            "main": "Clouds",
            "description": "scattered clouds",
            "icon": "03n"
          }
        ],
        "clouds": {
          "all": 48
        },
        "wind": {
          "speed": 4.06,
          "deg": 168
        },
        "visibility": 10000,
        "pop": 0,
        "sys": {
          "pod": "n"
        },
        "dt_txt": "2021-02-20 00:00:00"
      },
      {
        "dt": 1613822400,
        "main": {
          "temp": 14.68,
          "feels_like": 9.59,
          "temp_min": 14.68,
          "temp_max": 14.68,
          "pressure": 1011,
          "sea_level": 1011,
          "grnd_level": 1007,
          "humidity": 53,
          "temp_kf": 0
        },
        "weather": [
          {
            "id": 803,
            "main": "Clouds",
            "description": "broken clouds",
            "icon": "04d"
          }
        ],
        "clouds": {
          "all": 51
        },
        "wind": {
          "speed": 5.72,
          "deg": 169
        },
        "visibility": 10000,
        "pop": 0,
        "sys": {
          "pod": "d"
        },
        "dt_txt": "2021-02-20 12:00:00"
      },
      {
        "dt": 1613908800,
        "main": {
          "temp": 13.65,
          "feels_like": 9.5,
          "temp_min": 13.65,
          "temp_max": 13.65,
          "pressure": 1012,
          "sea_level": 1012,
          "grnd_level": 1007,
          "humidity": 52,
          "temp_kf": 0
        },
        "weather": [
          {
            "id": 804,
            "main": "Clouds",
            "description": "overcast clouds",
            "icon": "04d"
          }
        ],
        "clouds": {
          "all": 85
        },
        "wind": {
          "speed": 4.04,
          "deg": 155
        },
        "visibility": 10000,
        "pop": 0,
        "sys": {
          "pod": "d"
        },
        "dt_txt": "2021-02-21 12:00:00"
      },
      {
        "dt": 1613995200,
        "main": {
          "temp": 14.43,
          "feels_like": 11.13,
          "temp_min": 14.43,
          "temp_max": 14.43,
          "pressure": 1017,
          "sea_level": 1017,
          "grnd_level": 1012,
          "humidity": 51,
          "temp_kf": 0
        },
        "weather": [
          {
            "id": 803,
            "main": "Clouds",
            "description": "broken clouds",
            "icon": "04d"
          }
        ],
        "clouds": {
          "all": 64
        },
        "wind": {
          "speed": 2.95,
          "deg": 158
        },
        "visibility": 10000,
        "pop": 0,
        "sys": {
          "pod": "d"
        },
        "dt_txt": "2021-02-22 12:00:00"
      },
      {
        "dt": 1614081600,
        "main": {
          "temp": 14.09,
          "feels_like": 10.03,
          "temp_min": 14.09,
          "temp_max": 14.09,
          "pressure": 1030,
          "sea_level": 1030,
          "grnd_level": 1025,
          "humidity": 61,
          "temp_kf": 0
        },
        "weather": [
          {
            "id": 801,
            "main": "Clouds",
            "description": "few clouds",
            "icon": "02d"
          }
        ],
        "clouds": {
          "all": 19
        },
        "wind": {
          "speed": 4.7,
          "deg": 196
        },
        "visibility": 10000,
        "pop": 0,
        "sys": {
          "pod": "d"
        },
        "dt_txt": "2021-02-23 12:00:00"
      },
      {
        "dt": 1614168000,
        "main": {
          "temp": 16.01,
          "feels_like": 11.98,
          "temp_min": 16.01,
          "temp_max": 16.01,
          "pressure": 1026,
          "sea_level": 1026,
          "grnd_level": 1022,
          "humidity": 54,
          "temp_kf": 0
        },
        "weather": [
          {
            "id": 800,
            "main": "Clear",
            "description": "clear sky",
            "icon": "01d"
          }
        ],
        "clouds": {
          "all": 0
        },
        "wind": {
          "speed": 4.67,
          "deg": 177
        },
        "visibility": 10000,
        "pop": 0,
        "sys": {
          "pod": "d"
        },
        "dt_txt": "2021-02-24 12:00:00"
      }
    ],
    "city": {
      "id": 2988507,
      "name": city,
      "coord": {
        "lat": 48.8534,
        "lon": 2.3488
      },
      "country": "FR",
      "population": 2138551,
      "timezone": 3600,
      "sunrise": 1613717504,
      "sunset": 1613755047
    }
  });

}));