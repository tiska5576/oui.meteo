
abstract class Environment {

  // true when "flutter run" or "build" commands are launched with "--dart-define=TESTS=true" argument
  static const bool Tests = bool.fromEnvironment('TESTS');
  // true when "flutter run" or build commands are launched with "--dart-define=MOCK=true" argument
  static const bool Mock = bool.fromEnvironment('MOCK') || Tests;// || true;
  // true when "flutter run" or "build" commands are launched with "--prod" argument
  static const bool Prod = bool.fromEnvironment('dart.vm.product');

  static const String HttpEndpoint = Prod ? _HttpEndpointProd : _HttpEndpointDev;

  static const _HttpEndpointProd = "https://api.openweathermap.org/data/2.5";
  static const _HttpEndpointDev = "https://api.openweathermap.org/data/2.5";

  static const OpenWeatherApiKey = "a16ad734d11a3ae7d246dd61d0d3009f";
}
