
import 'package:oui_sncf_weather/constants/theme/dark.theme.dart';
import 'package:oui_sncf_weather/constants/theme/light.theme.dart';
import 'package:oui_sncf_weather/services/cache.dart';
import 'package:oui_sncf_weather/services/layout.dart';
import 'package:oui_sncf_weather/services/loading.dart';
import 'package:oui_sncf_weather/services/route.dart';
import 'package:oui_sncf_weather/services/theme.dart';
import 'package:oui_sncf_weather/services/translation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mvcprovider/mvcprovider.dart';
import 'package:provider/single_child_widget.dart';

import 'components/layout/root.widget.dart';
import 'constants/config/routes.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) => runApp(FlutterScaffoldModule()));
}

class FlutterScaffoldModule extends MVC_Module<MVC_Model, FlutterScaffoldView, MVC_Controller> {

  final FlutterScaffoldView view = FlutterScaffoldView();

  final List<SingleChildWidget> providers = [
    ThemeService().create,
    CacheService().create,
    LoadingService().create,
    LayoutService().create,
    RouteService().create,
    TranslationService().create,
  ];
}

class FlutterScaffoldView extends StatelessWidget with MVC_View<MVC_Model, MVC_Controller> {

  @override
  Widget build(BuildContext context) {

    return MaterialApp(
        theme: LightTheme,
        darkTheme: DarkTheme,
        themeMode: ThemeService(context).listen.mode,
        localizationsDelegates: TranslationService.delegates,
        supportedLocales: TranslationService.supportedLocales,
        title: "OUI.meteo",
        navigatorKey: RouteService.navigatorKey,
        initialRoute: RouteService.Initial,
        routes: Routes.Pages,
        debugShowCheckedModeBanner: false,
        builder: (BuildContext context, Widget child) => RootWidget(child: child)
    );
  }
}
