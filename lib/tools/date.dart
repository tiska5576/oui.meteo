
class DateTool {

  static DateTime now = DateTime.now();

  static String frenchDateFormat(DateTime date, {bool showYear = true}) {

    String day = addZeroIfMinorTen(date.day);
    String month = addZeroIfMinorTen(date.month);
    String year = addZeroIfMinorTen(date.year);

    return "$day/$month${showYear ? "/$year": ""}";

  }

  static String dayWord(DateTime date, Map<String, String> locale) {
    String day;
    if (locale != null) {
      DateTime today = removeTime(DateTime.now());
      int difference = today.difference(removeTime(date)).inDays;

      switch (difference) {
        case -1: day = locale["Tomorrow"]; break;
        case 0: day = locale["Today"]; break;
        case 1: day = locale["Yesterday"]; break;
        default:
          day = locale["day${date.weekday}"];
          break;
      }
    }
    return day;
  }

  static String toFrenchDate(DateTime date, { Map<String, String> locale }) {

    String day = frenchDateFormat(date);
    if (locale != null) day = dayWord(date, locale);

    String _day = addZeroIfMinorTen(date.day);
    String month = addZeroIfMinorTen(date.month);

    return "$day $_day/$month";
  }

  static String toFrenchDateTime(DateTime date, { Map<String, String> locale }) {

    String day = frenchDateFormat(date);
    if (locale != null) day = dayWord(date, locale);

    String hour = addZeroIfMinorTen(date.hour);
    String minute = addZeroIfMinorTen(date.minute);

    return "$day ${locale["at"]} ${hour}h$minute";

  }

  static String toTime(DateTime date, {bool showMs = false, bool showS = true}) {

    String hour = addZeroIfMinorTen(date.hour);
    String minute = addZeroIfMinorTen(date.minute);
    String second = addZeroIfMinorTen(date.second);
    String ms = addTwoZerosIfMinorHundred(date.millisecond);

    return "${hour}h$minute${showMs ? ":$second.$ms" : showS ? ":${second}s": ""}";

  }

  static String getTimeDifference(DateTime start, DateTime stop) {
    Duration difference = stop.difference(start);
    int hours = difference.inHours;
    int minutes = difference.inMinutes % 60;
    int seconds = difference.inSeconds % 60;

    return (hours + minutes) > 0 ? "${addZeroIfMinorTen(hours)}h${addZeroIfMinorTen(minutes)}" : "${seconds}s";
  }

  static String addTwoZerosIfMinorHundred(int nb) {
    return nb < 100 && nb >= 10 ? "0" + nb.toString() : addZeroIfMinorTen(nb);
  }

  static String addZeroIfMinorTen(int nb) => nb < 10 && nb >= 0 ? "0" + nb.toString() : nb.toString();

  static DateTime removeTime(DateTime date) => DateTime(date.year, date.month, date.day);

  static DateTime addCurrentTimeZone(DateTime date) => date.add(now.timeZoneOffset);

  static DateTime removeCurrentTimeZone(DateTime date) => date.add(-now.timeZoneOffset);
}