
class UserDataModel {

  final int id;
  String sessionToken;
  final String lastName;
  final String firstName;
  final String email;
  final String password;
  final String avatarUrl;

  UserDataModel({
    this.id,
    this.sessionToken,
    this.lastName,
    this.firstName,
    this.email,
    this.password,
    this.avatarUrl,
  });

  static UserDataModel fromJson(Map<String, dynamic> data) {

    if (data == null) return null;

    return UserDataModel(
        id: data["id"],
        lastName: data["lastName"],
        firstName: data["firstName"],
        email: data["email"],
        password: data["password"],
        avatarUrl: data["avatarUrl"],
        sessionToken: data["sessionToken"],
    );
  }

  static int _mockedId = 1;
  static UserDataModel mocked(String lastName, String firstName, String avatarUrl) {

    return UserDataModel(
      id: _mockedId++,
      lastName: lastName,
      firstName: firstName,
      email: "${firstName.toLowerCase()}.${lastName.toLowerCase()}@gmail.com",
      password: "1234",
      avatarUrl: avatarUrl,
    );
  }
}