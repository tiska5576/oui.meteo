
import 'package:oui_sncf_weather/constants/i18n/en.dart';
import 'package:oui_sncf_weather/constants/i18n/fr.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:mvcprovider/mvcprovider.dart';

class TranslationService extends MVC_ProxyProvider<TranslationService, Map<String, String>> {

  final Locale locale;

  TranslationService([context, this.locale]) : super(context);

  static Map<String, Map<String, String>> _localizedValues = {
    'en': ENGLISH,
    'fr': FRENCH,
  };

  static List<String> supportedLanguages = _localizedValues.keys.toList();
  static List<Locale> supportedLocales = supportedLanguages.map((code) => Locale(code, '')).toList();
  static List<LocalizationsDelegate> delegates = [
    const LocalizationServiceDelegate(),
    GlobalMaterialLocalizations.delegate,
    GlobalWidgetsLocalizations.delegate,
    GlobalCupertinoLocalizations.delegate,
  ];

  Map<String , String> get translate => _localizedValues[locale.languageCode];

  @override
  Map<String, String> builder(BuildContext context) {
    return Localizations.of<TranslationService>(context, TranslationService).translate;
  }
}

class LocalizationServiceDelegate extends LocalizationsDelegate<TranslationService> {
  const LocalizationServiceDelegate();

  @override
  bool isSupported(Locale locale) => TranslationService.supportedLanguages.contains(locale.languageCode);

  @override
  Future<TranslationService> load(Locale locale) {
    // Returning a SynchronousFuture here because an async "load" operation
    // isn't needed to produce an instance of DemoLocalizations.
    return SynchronousFuture<TranslationService>(TranslationService(null, locale));
  }

  @override
  bool shouldReload(LocalizationServiceDelegate old) => false;
}
