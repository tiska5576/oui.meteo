
import 'package:auto_size_text/auto_size_text.dart';
import 'package:oui_sncf_weather/components/labels/error.label.dart';
import 'package:oui_sncf_weather/components/layout/custom.container.dart';
import 'package:oui_sncf_weather/constants/theme/theme.colors.dart';
import 'package:oui_sncf_weather/constants/theme/theme.sizes.dart';
import 'package:oui_sncf_weather/tools/log.dart';
import 'package:oui_sncf_weather/tools/text.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:mvcprovider/mvcprovider.dart';
import 'package:provider/provider.dart';

class LayoutService extends MVC_Notifier<LayoutService> {

  static final toastCharacterLimit = 100;
  FToast fToast;
  Size deviceSize;

  LayoutService([context]) : super(context);

  @override
  ChangeNotifierProvider<LayoutService> get create {
    fToast = FToast()..init(context);
    return super.create;
  }

  @override
  LayoutService update(BuildContext context) {
    fToast.init(context);
    return super.update(context);
  }

  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  ScaffoldState get scaffold => scaffoldKey.currentState;

  bool _shouldRotateBackground = false;
  bool get shouldRotateBackground => _shouldRotateBackground;
  set shouldRotateBackground(bool value) {
    if (value != shouldRotateBackground) {
      _shouldRotateBackground = value;
    }
  }

  Widget _floatingActionButton;
  Widget get floatingActionButton => _floatingActionButton;
  set floatingActionButton(Widget value) {
    if (value != floatingActionButton) {
      _floatingActionButton = value;
      notifyListeners();
    }
  }

  Future<void> alert({String title, dynamic body, Map<String, Function> buttons, int maxBodyLines = 10, bool barrierDismissible = false}) async {

    return await showDialog(
      context: context,
      barrierDismissible: barrierDismissible,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title),
          content: Container(
            height: screenHeight * 1/5,
            width: screenWidth * 2/3,
            child: SingleChildScrollView(
              child: ListBody(
                children: <Widget>[
                  if (body is String) AutoSizeText(body, maxLines: maxBodyLines),
                  if (body is Widget) body,
                ],
              ),
            ),
          ),
          actions: buttons.keys.map((key) => TextButton(
                child: Text(key),
                onPressed: () => buttons[key](),
              )).toList()
        );
      },
    );
  }

  void showToast(dynamic toast, [ToastGravity gravity = ToastGravity.BOTTOM, int duration = 3]) {
    if (toast == null) return;
    if (toast is String) {
      Log.print("show toast: ${TextTool.limitNbOfCharacters(toast, toastCharacterLimit)}");
      toast = Padding(
        padding: ThemeSizes.paddingSymmetric[Sizes.XS][Sizes.Zero],
        child: CustomContainer(
          color: Theme.of(context).primaryColorDark,
          child: Padding(
            padding: ThemeSizes.padding[Sizes.M],
            child: ErrorLabel(TextTool.limitNbOfCharacters(toast, toastCharacterLimit), color: ThemeColors.Light),
          ),
        ),
      );
    }

    assert(toast is Widget);

    fToast.showToast(
        child: toast,
        gravity: gravity,
        toastDuration: Duration(seconds: duration)
    );
  }

  toggleSideMenu() async {
    if (scaffoldKey.currentState.isDrawerOpen) {
      scaffoldKey.currentState.openEndDrawer();
    } else {
      scaffoldKey.currentState.openDrawer();
    }
  }

  double get screenWidth {
    double width = MediaQuery.of(context).size.width;
    EdgeInsets padding = MediaQuery.of(context).padding;
    return width - padding.horizontal;
  }

  double get screenHeight {
    double height = MediaQuery.of(context).size.height;
    EdgeInsets padding = MediaQuery.of(context).padding;
    return height - padding.top - kToolbarHeight;
  }
}