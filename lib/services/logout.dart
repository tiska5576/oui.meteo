
import 'package:oui_sncf_weather/constants/config/routes.dart';
import 'package:flutter/material.dart';
import 'package:oui_sncf_weather/services/route.dart';
import 'package:oui_sncf_weather/tools/log.dart';

import 'cache.dart';

class LogoutService {

  static exit(BuildContext context) async {

    Log.print('Logging out', title: 'LogoutService');

    CacheService(context).instance.empty();

    RouteService(context).goTo(Routes.Login, params: {RouteParams.LoggedOut: true}, removeUntil: true);
  }
}