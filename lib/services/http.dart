// ignore_for_file: non_constant_identifier_names
import 'dart:convert';
import 'package:oui_sncf_weather/services/mock.dart';
import 'package:oui_sncf_weather/tools/text.dart';
import 'package:flutter/cupertino.dart';
import 'package:oui_sncf_weather/constants/config/environment.dart';
import 'package:oui_sncf_weather/services/layout.dart';
import 'package:oui_sncf_weather/services/local.storage.dart';
import 'package:oui_sncf_weather/tools/log.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';

import 'error.dart';

class HttpService {

  BuildContext context;

  HttpService(this.context);

  static final Client _client = Client();

  static final int Timeout = 30; // In seconds

  static final Map<HTTPMethods, Function> Call = {
    HTTPMethods.POST: _client.post,
    HTTPMethods.GET: (url, {Map<String, String> headers, body}) => _client.get(url, headers: headers),
    HTTPMethods.PUT: _client.put,
    HTTPMethods.DELETE: _client.delete,
    HTTPMethods.MOCK: MockService.call
  };

  Future<T> get<T>({
    String url,
    String path,
    Map<String, dynamic> params,
    Map<String, String> headers,
    T Function(Map<String, dynamic>) factory,
    HTTPMethods method = HTTPMethods.POST,
    String responseProperty,
    bool paramsAsString = false,
    bool force = true,
    bool cache = false,
    Function onFail,
    Function(Response) onResponse,
    Function(T) onSuccess,
  }) async {

    Map<String, dynamic> parsedResponse = await HttpRequest<Map<String, dynamic>>(
        url: url,
        path: path,
        params: params,
        paramsAsString: paramsAsString,
        headers: headers,
        method: method,
        responseProperty: responseProperty,
        force: force,
        cache: cache,
        onFail: onFail,
        onResponse: onResponse,
        context: context
    ).run();

    try {
      T result = parsedResponse != null ? factory(parsedResponse) : null;
      if (onSuccess != null) onSuccess(result);
      return result;
    } catch(e) {
      Log.print("Http request error while executing ${T.toString()} factory", title: 'HttpService', error: e, stackTrace: true);
      return null;
    }
  }

  Future<List<T>> getList<T>({
    String url,
    String path,
    Map<String, dynamic> params,
    Map<String, String> headers,
    T Function(Map<String, dynamic>) factory,
    HTTPMethods method = HTTPMethods.POST,
    String responseProperty,
    bool paramsAsString = false,
    bool force = true,
    bool cache = false,
    Function onFail,
    Function(Response) onResponse,
    Function(List<T>) onSuccess,
  }) async {

    List<dynamic> parsedResponse = await HttpRequest<List<dynamic>>(
        url: url,
        path: path,
        params: params,
        paramsAsString: paramsAsString,
        headers: headers,
        method: method,
        responseProperty: responseProperty,
        force: force,
        cache: cache,
        onFail: onFail,
        onResponse: onResponse,
        context: context
    ).run();

    try {
      List<T> result = parsedResponse != null ? parsedResponse.map((data) => factory(data)).toList() : null;
      if (onSuccess != null) onSuccess(result);
      return result;
    } catch(e) {
      Log.print("Request error while executing List<${T.toString()}> factory", title: 'HttpService', error: e);
      return null;
    }
  }
}

class HttpRequest<T> {

  String url;
  String path;
  Map<String, dynamic> params;
  Map<String, String> headers;
  Future<T> Function<T>(Response response, [String responseProperty]) parser;
  HTTPMethods method = HTTPMethods.POST;
  String responseProperty;
  bool paramsAsString = false;
  bool force = true;
  bool cache = false;
  Function onFail;
  Function(Response) onResponse;
  BuildContext context;

  HttpRequest({
    this.url,
    this.path,
    this.params,
    this.headers,
    this.parser,
    this.method,
    this.responseProperty,
    this.paramsAsString,
    this.force,
    this.cache,
    this.onFail,
    this.onResponse,
    this.context
  }) {

    url ??= Environment.HttpEndpoint;
    path ??= "";
    params ??= const {};
    parser ??= _defaultParser;

    path += method == HTTPMethods.GET ? _addGetParams(params) : "";
  }

  T result;
  String stored;
  String error;

  static const int LogNbOfCharactersLimit = 1000;
  bool get isMock => Environment.Mock || method == HTTPMethods.MOCK;
  String get logTitle => "${isMock ? "MOCKED " : ""}HttpService";
  String get storageKey => "$url$path${params.toString()}";

  Future<T> run() async {
    try {

      if (!force) stored = await LocalStorageService.get<String>(storageKey);
      if (stored != null) result = json.decode(stored);
      else {
        if (Environment.Mock || method == HTTPMethods.MOCK) {
          method = HTTPMethods.MOCK;
        }
        Log.print("Requesting ${T.toString()} : $url$path,\n\nparams: ${params.toString()},\n\nheaders : ${headers.toString()}", title: logTitle);

        Response response = await HttpService.Call[method](url + path, body: paramsAsString ? jsonEncode(params) : params, headers: headers, )
            .timeout(Duration(seconds: HttpService.Timeout), onTimeout: () {
          Log.print("Requesting <${T.toString()}>", title: logTitle, error: "Request timeout");
          LayoutService(context).instance.showToast("Request canceled because too long. Try again later please.");
          return null;
        });

        if (response != null) {
          result = await parser<T>(response, responseProperty);
          if (onResponse != null) onResponse(response);
        }
      }
    }
    catch(e) {
      String error = e.toString().replaceFirst("Exception: ", "");
      if (ErrorService.errorHandlers.containsKey(error)) {
        return await ErrorService.errorHandlers[error](this);
      }
      Log.print("Request error while requesting or parsing <${T.toString()}>", title: logTitle, error: error);
      ErrorService.defaultErrorHandler(context, error);
      if (onFail != null) onFail();
      return null;
    }

    if (result != null && cache && (force || stored == null)) await LocalStorageService.store("$url$path${params.toString()}", json.encode(result));

    return result;
  }

  // Transform POST params into GET url params
  String _addGetParams(Map<String, dynamic> params) {
    if (params.length == 0) return "";
    String result = "?";
    params.forEach((key, value) {
      result += "$key=$value&";
    });
    return result.substring(0, result.length - 1);
  }

  // Transform an object Response into the given type
  Future<T> _defaultParser<T>(Response response, [String responseProperty]) async {
    T result;
    try {
      if (_isEmpty(response)) return result;
      Log.print("Parsing ${T.toString()} :\n\n${response.body.toString()}", title: logTitle, limit: LogNbOfCharactersLimit);
      result = responseProperty != null ? json.decode(response.body) != null ? json.decode(response.body)[responseProperty] : json.decode(response.body) : json.decode(response.body);
    } catch(e) {
      Log.print("Request error while parsing response", title: logTitle, error: e, stackTrace: true);
      return null;
    }
    handleResponseError(response, result);
    return result;
  }

  // Throw and exception with an error message if there is an error
  void handleResponseError(Response response, [dynamic parsedResponse]) {
    parsedResponse ??= response?.body;

    if (parsedResponse != null && !(parsedResponse is List<dynamic>)) {
      parsedResponse = parsedResponse is String ? json.decode(parsedResponse) : parsedResponse as Map<String, dynamic>;

      if (response.statusCode != 200 || (parsedResponse != null
          && (parsedResponse["error"] != null
              || parsedResponse["errorMessage"] != null))) {
        // Check server message error
        if (parsedResponse != null) {
          if (parsedResponse["error"] != null) {
            error = "${parsedResponse["error"]}";
          }
          if (parsedResponse["errorMessage"] != null) {
            error = "${parsedResponse["errorMessage"]}";
          }
          if (error != null) {
            if (parsedResponse["errorCode"] != null) {
              error = "${parsedResponse["errorCode"]} - $error";
            }
          }
          else error = "Error : ${response.body}";
        }
        else error = "Error : ${response.body}";
      }
    } else if (response?.statusCode != 200) {
      error = "Response status ${response.statusCode}";
    }

    if (TextTool.notNull(error)) throw Exception(error);
  }

  bool _isEmpty(dynamic response) => response == null || response.body == null || (response.body != null && response.body.length == 0);

}

enum HTTPMethods { POST, GET, PUT, DELETE, MOCK }
