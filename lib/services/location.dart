import 'package:oui_sncf_weather/constants/config/environment.dart';
import 'package:oui_sncf_weather/services/cache.dart';
import 'package:oui_sncf_weather/services/layout.dart';
import 'package:oui_sncf_weather/services/mock.dart';
import 'package:oui_sncf_weather/services/translation.dart';
import 'package:oui_sncf_weather/tools/log.dart';
import 'package:geocoder/geocoder.dart';
import 'package:geolocator/geolocator.dart';
import 'package:mvcprovider/mvcprovider.dart';

class LocationService extends MVC_Notifier<LocationService> {

  Position _position;
  LocationPermission _permission;

  LocationService([context]) : super(context);

  CacheService get cacheService => CacheService(context).instance;

  Future<Position> get position async {
    _position ??= await refreshPosition();
    return _position;
  }

  Coordinates positionToCoordinates(Position pos) {
    return Coordinates(pos.latitude, pos.longitude);
  }

  Future<Address> findAddress([Position pos]) async {
    Coordinates _coordinates = positionToCoordinates(pos ?? await position);
    Address result = (await Geocoder.local.findAddressesFromCoordinates(_coordinates) ?? []).first;
    Log.print("Reverse geocoding: ${result?.addressLine}", title: "LocationService");
    return result;
  }

  Future<bool> get permission async {
    if (!await Geolocator.isLocationServiceEnabled()) {
      LayoutService(context).instance.showToast(TranslationService(context).get["Thank you to enable location service"]);
      return null;
    }

    if (_permission == LocationPermission.always || _permission == LocationPermission.whileInUse) {
      return true;
    }
    else _permission = await Geolocator.checkPermission();

    if (_permission == LocationPermission.denied) {
      _permission = await Geolocator.requestPermission();
    }

    if (_permission == LocationPermission.deniedForever || _permission == LocationPermission.denied) {
      LayoutService(context).instance.showToast(TranslationService(context).get["Thank you to grant location permission to our application in your device settings"]);
    }

    return _permission == LocationPermission.always || _permission == LocationPermission.whileInUse;
  }

  Future<Position> refreshPosition() async {

    if (Environment.Mock) {
      _position = Position(
          latitude: MockService.Latitude,
          longitude: MockService.Longitude
      );
    }
    else if (await permission) try {
      _position = await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.best, timeLimit: Duration(seconds: 5));
      Log.print("Retrieved current position: $_position", title: "LocationService");
    } catch (e) {
      _position = await Geolocator.getLastKnownPosition();
      Log.print("Fallback to the last known position: $_position", title: "LocationService");
    }

    return _position;
  }
}

