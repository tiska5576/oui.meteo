
import 'package:oui_sncf_weather/constants/config/test.keys.dart';
import 'package:oui_sncf_weather/services/route.dart';
import 'package:flutter/material.dart';
import 'app.bar.button.dart';


class CustomBackButton extends AppBarButton {

  CustomBackButton({Future Function() onBack}) : super(
    icon: Icons.arrow_back_ios_rounded,
    hero: "BACK_BUTTON",
    testKey: TestKey(key: Keys.BackButton),
    action: (BuildContext context) async {
      if (onBack != null) await onBack();
      RouteService(context).instance.pop();
    },
  );
}