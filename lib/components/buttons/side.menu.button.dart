
import 'package:flutter/material.dart';
import 'package:oui_sncf_weather/components/labels/subtitle.label.dart';
import 'package:oui_sncf_weather/constants/config/test.keys.dart';
import 'package:oui_sncf_weather/constants/theme/theme.sizes.dart';
import 'package:oui_sncf_weather/services/layout.dart';

class SideMenuButton extends StatelessWidget {

  SideMenuButton({
    this.action,
    this.testKey,
    this.title,
    this.icon,
  });

  final Function action;
  final String testKey;
  final String title;
  final IconData icon;

  @override
  Widget build(BuildContext context) {
    return  ListTile(
      key: Key(testKey ?? TestKey(key: Keys.SideMenuButton, id: title)),
      title: SubtitleLabel(title),
      minLeadingWidth: ThemeSizes.paddingValue[Sizes.S],
      leading: Icon(
          icon ?? Icons.arrow_forward_ios,
          color: Theme.of(context).iconTheme.color
      ),
      onTap: () {
        action();
        LayoutService(context).instance.toggleSideMenu();
      },
    );
  }
}