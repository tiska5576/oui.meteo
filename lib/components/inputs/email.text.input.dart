
import 'package:oui_sncf_weather/constants/theme/theme.sizes.dart';
import 'package:flutter/material.dart';
import 'package:oui_sncf_weather/tools/regex.dart';

import 'custom.text.input.dart';

class EmailTextInput extends StatelessWidget {

  EmailTextInput({ this.model, this.testKey });

  final EmailTextInputModel model;
  final String testKey;

  @override
  Widget build(BuildContext context) {
    EmailTextInputModel _model = model ?? EmailTextInputModel();
    return CustomTextInput(model: _model, testKey: testKey);
  }
}

class EmailTextInputModel extends CustomTextInputModel {

  EmailTextInputModel({String value, dynamic next})
      : super(value: value, next: next);

  @override
  String get label => translate["E-mail"];
  @override
  String get invalidMessage => translate["Invalid e-mail"];
  @override
  String get errorMessage => translate["Incorrect credentials"];

  final bool secret = false;
  final bool required = true;
  final TextInputType keyboard = TextInputType.emailAddress;
  final Icon icon = Icon(Icons.mail, size: ThemeSizes.icon[Sizes.M]);
  final TextAlign textAlign = TextAlign.center;

  @override
  bool validate() => super.validate() && Regex.email.hasMatch(value);
}