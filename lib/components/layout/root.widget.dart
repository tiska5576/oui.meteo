
import 'package:oui_sncf_weather/constants/theme/theme.colors.dart';
import 'package:flutter/material.dart';
import 'package:oui_sncf_weather/services/cache.dart';
import 'package:oui_sncf_weather/services/layout.dart';
import 'package:oui_sncf_weather/services/theme.dart';

import 'side.menu.dart';

class RootWidget extends StatelessWidget {

  final Widget child;

  RootWidget({this.child});

  @override
  Widget build(BuildContext context) {
    LayoutService layoutService = LayoutService(context).listen;
    bool isDark = ThemeService(context).instance.isDark;

    return Scaffold(
      key: layoutService.scaffoldKey,
      body: Stack(
        alignment: Alignment.center,
        children: [
          Container(
            color: isDark ? ThemeColors.Dark : ThemeColors.Light,
        ),
          child
        ],
      ),
      drawer: CacheService(context).listen.isLogged ? SideMenu() : null,
      floatingActionButton: layoutService.floatingActionButton,
    );
  }
}