
import 'package:flutter/material.dart';
import 'package:oui_sncf_weather/constants/theme/theme.sizes.dart';

import 'custom.label.dart';

class ErrorLabel extends CustomLabel {

  ErrorLabel(text, {
    heroTag,
    maxLines = 100,
    expand = false,
    align,
    color,
    size,
    overflow = TextOverflow.visible,
    wrap = true,
    testKey
  }) : super(text,
      heroTag: heroTag,
      maxLines: maxLines,
      expand: expand,
      align: align,
      color: color,
      size: size,
      overflow: overflow,
      wrap: wrap,
      testKey: testKey
  );

  @override
  TextStyle getTextStyle(BuildContext context) {
    TextStyle result;

    switch(size) {
      case Sizes.L: result = Theme.of(context).textTheme.bodyText1; break;
      case Sizes.M: result = Theme.of(context).textTheme.bodyText2; break;
      default: result = Theme.of(context).textTheme.bodyText2; break;
    }

    return result.copyWith(color: color ?? Theme.of(context).errorColor);
  }
}