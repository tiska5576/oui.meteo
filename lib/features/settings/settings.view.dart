
import 'package:flutter/material.dart';
import 'package:oui_sncf_weather/components/buttons/ok.button.dart';
import 'package:oui_sncf_weather/components/buttons/simple.button.dart';
import 'package:oui_sncf_weather/components/inputs/custom.form.dart';
import 'package:oui_sncf_weather/components/labels/simple.label.dart';
import 'package:oui_sncf_weather/components/labels/subtitle.label.dart';
import 'package:oui_sncf_weather/components/layout/custom.sliver.bar.dart';
import 'package:oui_sncf_weather/components/layout/scroll.page.dart';
import 'package:oui_sncf_weather/constants/theme/theme.sizes.dart';
import 'package:oui_sncf_weather/features/settings/components/city.text.input.dart';
import 'package:oui_sncf_weather/services/theme.dart';
import 'package:oui_sncf_weather/tools/text.dart';
import 'package:mvcprovider/mvcprovider.dart';

import 'settings.ctrl.dart';
import 'settings.model.dart';

class SettingsView extends StatelessWidget with MVC_View<SettingsModel, SettingsCtrl>  {

  @override
  Widget build(BuildContext context) {
    listen(context);
    bool isDark = ThemeService(context).listen.isDark;

    return ScrollPage(
      overlap: false,
      slivers: <Widget>[
        CustomSliverBar(
          title: SubtitleLabel(TextTool.capitalize(model.title), heroTag: "TITLE-${model.title}"),
        ),
      ],
      child: Padding(
        padding: ThemeSizes.padding[Sizes.L],
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CustomForm(
              key: ctrl.formKey,
              onSubmit: ctrl.onSubmitCity,
              inputModels: [
                model.tagInput
              ],
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(child: CityTextInput(model: model.tagInput)),
                  Padding(
                    padding: ThemeSizes.paddingOnly[AxisDirection.left][Sizes.M],
                    child: OkButton(
                      padding: ThemeSizes.padding[Sizes.Zero],
                      action: () => ctrl.formKey.currentState.submit(),
                    ),
                  )
                ],
              ),
            ),
            Padding(
              padding: ThemeSizes.paddingOnly[AxisDirection.down][Sizes.L],
              child:
              Wrap(
                  children: model.cache.weatherCities.map((city) {
                    return Padding(
                      padding: ThemeSizes.padding[Sizes.XXS],
                      child: ActionChip(
                        onPressed: () => ctrl.removeCity(city),
                        label: Text(city),
                      ),
                    );
                  }).toList()
              ),
            ),
            Padding(
                padding: ThemeSizes.paddingOnly[AxisDirection.down][Sizes.L],
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SimpleLabel("${model.translate["Theme"]} : ", size: Sizes.L),
                    SimpleButton(
                      action: ctrl.onToggleTheme,
                      child: Row(
                        children: [
                          Icon(isDark ? Icons.wb_sunny : Icons.nightlight_round),
                          Padding(
                            padding: ThemeSizes.paddingOnly[AxisDirection.left][Sizes.XS],
                            child: Text(isDark
                                ? model.translate["Light"]
                                : model.translate["Dark"]
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                )
            ),
          ],
        ),
      ),
    );
  }
}