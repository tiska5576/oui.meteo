
import 'package:oui_sncf_weather/components/inputs/custom.text.input.dart';
import 'package:oui_sncf_weather/constants/config/test.keys.dart';
import 'package:oui_sncf_weather/constants/theme/theme.sizes.dart';
import 'package:flutter/material.dart';
import 'package:oui_sncf_weather/services/translation.dart';

class CityTextInput extends StatelessWidget {

  CityTextInput({ this.model });

  final CityTextInputModel model;
  final String testKey = TestKey(feature: Features.Settings, key: Keys.TextInput);

  @override
  Widget build(BuildContext context) {
    CityTextInputModel _model = model ?? CityTextInput();
    return CustomTextInput(model: _model, testKey: testKey);
  }
}

class CityTextInputModel extends CustomTextInputModel {

  CityTextInputModel({String value, dynamic next}) : super(value: value, next: next);

  int minLength = 3;
  Map<String, String> get translate => TranslationService(context).instance;

  @override
  String get label => translate["Add a city here"];
  @override
  String get invalidMessage => minLength.toString() + " " + translate["characters minimum"];
  @override
  String get errorMessage => "Omfg";

  final bool required = true;
  final Icon icon = Icon(Icons.tag, size: ThemeSizes.icon[Sizes.M]);
  final TextAlign textAlign = TextAlign.center;

  @override
  bool validate() => super.validate() && value.length >= minLength;
}