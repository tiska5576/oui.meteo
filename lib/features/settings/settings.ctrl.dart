
import 'package:flutter/cupertino.dart';
import 'package:oui_sncf_weather/components/inputs/custom.form.dart';
import 'package:oui_sncf_weather/services/loading.dart';
import 'package:oui_sncf_weather/services/theme.dart';
import 'package:mvcprovider/mvcprovider.dart';

import 'settings.model.dart';

class SettingsCtrl extends MVC_Controller<SettingsModel> {

  final GlobalKey<CustomFormState> formKey = GlobalKey<CustomFormState>();
  LoadingService get loading => LoadingService(context).instance;
  ThemeService get theme => ThemeService(context).instance;

  Future<void> onSubmitCity() async {
    loading.start(model.loadingId);
    await model.cache.addWeatherCity(model.tagInput.value);
    model.tagInput.value = null;
    loading.stop(model.loadingId);
    notifyListeners();
  }

  Future<void> removeCity(String city) async {
    loading.start(model.loadingId);
    await model.cache.removeWeatherCity(city);
    model.tagInput.value = null;
    loading.stop(model.loadingId);
    notifyListeners();
  }

  void onToggleTheme() => theme.toggleMode();
  dynamic onThemeSelect(dynamic value) => theme.selectTheme(value);

}