
import 'package:flutter/material.dart';
import 'package:oui_sncf_weather/constants/config/routes.dart';
import 'package:oui_sncf_weather/features/home/home.ctrl.dart';
import 'package:oui_sncf_weather/services/cache.dart';
import 'package:mvcprovider/mvcprovider.dart';

class HomeModel extends MVC_Model<HomeCtrl> {

  List<Widget> tabViews;
  int currentTabIndex = 0;
  CacheService get cache => CacheService(context).instance;

  void initTabs([bool shouldRefresh = false]) {
    tabViews = Routes.Weather.values.map((city) => Routes.Pages[city](context)).toList();
    if (shouldRefresh) {
      currentTabIndex = currentTabIndex >= tabViews.length ? 0 : currentTabIndex;
      notifyListeners();
    }
  }

  @override
  void init() => initTabs();
}