
import 'package:oui_sncf_weather/constants/config/routes.dart';
import 'package:oui_sncf_weather/services/cache.dart';
import 'package:oui_sncf_weather/services/route.dart';
import 'package:mvcprovider/mvcprovider.dart';
import 'home.model.dart';
import 'home.ctrl.dart';
import 'home.view.dart';

class HomePage extends MVC_Module<HomeModel, HomeView, HomeCtrl> {

  static final HomeModel _model = HomeModel();

  final HomeModel model = _model;
  final HomeView view = HomeView();
  final HomeCtrl ctrl = HomeCtrl();

  static final List<Guard> guards = [
        (_context, destination) async {
      List<String> cities = await CacheService(_context).get.getWeatherCities();
      Routes.initWeatherRoutes(cities);
      return cities != null;
    },
  ];

  static final List<Guard> onPop = [
    (_context, destination) async {
      _model.initTabs(true);
      return true;
    },
  ];
}