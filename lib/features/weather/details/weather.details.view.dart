
import 'package:flutter/material.dart';
import 'package:oui_sncf_weather/components/labels/subtitle.label.dart';
import 'package:oui_sncf_weather/components/labels/title.label.dart';
import 'package:oui_sncf_weather/components/layout/custom.sliver.bar.dart';
import 'package:oui_sncf_weather/components/layout/scroll.page.dart';
import 'package:oui_sncf_weather/constants/theme/theme.sizes.dart';
import 'package:oui_sncf_weather/services/translation.dart';
import 'package:mvcprovider/mvcprovider.dart';
import 'package:oui_sncf_weather/tools/date.dart';

import 'components/weather.detail.item.dart';
import 'weather.details.ctrl.dart';
import 'weather.details.model.dart';

class WeatherDetailsView extends StatelessWidget with MVC_View<WeatherDetailsModel, WeatherDetailsCtrl> {

  @override
  Widget build(BuildContext context) {
    listen(context);

    return ScrollPage(
      slivers: [
        CustomSliverBar(
          title: SafeArea(
            child: Center(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: ThemeSizes.paddingOnly[AxisDirection.right][Sizes.S],
                    child: Hero(
                        tag: 'TITLE-${model.weather.id}}',
                        child: TitleLabel(DateTool.dayWord(model.weather.date, TranslationService(context).instance))
                    ),
                  ),
                  Hero(
                      tag: 'DATE-${model.weather.id}}',
                      child: SubtitleLabel(DateTool.frenchDateFormat(model.weather.date, showYear: false), size: Sizes.M)
                  ),
                ],
              ),
            ),
          ),
          padding: ThemeSizes.padding[Sizes.Zero],
        ),
        SliverList(
          delegate: SliverChildBuilderDelegate(
                  (context, index) => WeatherDetailItem(model.weathers[index]),
              childCount: model.weathers.length
          ),
        ),
      ],
    );
  }
}