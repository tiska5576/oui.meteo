
import 'package:oui_sncf_weather/constants/config/routes.dart';
import 'package:oui_sncf_weather/models/weather.data.dart';
import 'package:mvcprovider/mvcprovider.dart';
import 'package:oui_sncf_weather/services/cache.dart';
import 'package:oui_sncf_weather/services/route.dart';

class WeatherDetailsModel extends MVC_Model {

  String city;
  WeatherDataModel weather;
  List<WeatherDataModel> weathers;

  CacheService get cacheService => CacheService(context).instance;
  RouteService get routeService => RouteService(context).instance;

  @override
  void init() {
    String weatherId = routeService.params[RouteParams.ItemId];
    city = routeService.params[RouteParams.City];
    weather = cacheService.weathers[weatherId];
    weathers = cacheService.weathersByCity[city].values
        .where((_weather) => _weather.date.day == weather.date.day
              && _weather.date.month == weather.date.month
        ).toList();
  }
}