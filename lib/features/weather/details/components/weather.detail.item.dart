
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:oui_sncf_weather/components/labels/custom.label.dart';
import 'package:oui_sncf_weather/components/labels/subtitle.label.dart';
import 'package:oui_sncf_weather/components/labels/title.label.dart';
import 'package:oui_sncf_weather/components/layout/custom.container.dart';
import 'package:oui_sncf_weather/constants/theme/theme.colors.dart';
import 'package:oui_sncf_weather/constants/theme/theme.sizes.dart';
import 'package:oui_sncf_weather/models/weather.data.dart';
import 'package:oui_sncf_weather/services/theme.dart';
import 'package:oui_sncf_weather/services/translation.dart';
import 'package:oui_sncf_weather/tools/date.dart';
import 'package:oui_sncf_weather/tools/text.dart';

class WeatherDetailItem extends StatelessWidget {

  final WeatherDataModel weather;

  WeatherDetailItem(this.weather);

  @override
  Widget build(BuildContext context) {

    Map<String, String> translate = TranslationService(context).get;
    bool isDark = ThemeService(context).instance.isDark;

    return Padding(
      padding: ThemeSizes.padding[Sizes.M],
      child: CustomContainer(
        child: Padding(
          padding: ThemeSizes.padding[Sizes.S],
          child: Row(
              children: [
                Column(
                  children: [
                    Hero(
                      tag: 'IMAGE-${weather.id}}',
                      child: CircleAvatar(
                          backgroundColor: isDark ? ThemeColors.AvatarColorDark : ThemeColors.AvatarColor,
                          backgroundImage: NetworkImage(weather.iconUrl)
                      ),
                    ),
                    Padding(
                      padding: ThemeSizes.paddingOnly[AxisDirection.up][Sizes.S],
                      child: CustomLabel(DateTool.toTime(weather.date, showS: false), style: Theme.of(context).textTheme.overline),
                    )
                  ],
                ),
                Expanded(
                  child: Padding(
                    padding: ThemeSizes.paddingOnly[AxisDirection.left][Sizes.M],
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        TitleLabel("${weather.temperature.toInt()}°", size: Sizes.M),
                        Hero(
                            tag: 'SUBTITLE-${weather.id}}',
                            child: SubtitleLabel(TextTool.capitalize(translate[weather.description]), size: Sizes.S)
                        ),
                      ],
                    ),
                  ),
                ),
                Column(
                  children: [
                    Transform.rotate(
                      angle: weather.windDirection * pi/180,
                       child: Icon(Icons.arrow_upward, size: ThemeSizes.icon[Sizes.M])
                    ),
                    Padding(
                      padding: ThemeSizes.paddingOnly[AxisDirection.up][Sizes.S],
                      child: CustomLabel("${(weather.windSpeed * 3.6).toInt()} km/h", style: Theme.of(context).textTheme.overline),
                    )
                  ],
                ),
              ],
            ),
        ),
      )
    );
  }



}