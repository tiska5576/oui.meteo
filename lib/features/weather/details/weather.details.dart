
import 'package:mvcprovider/mvcprovider.dart';

import 'weather.details.ctrl.dart';
import 'weather.details.model.dart';
import 'weather.details.view.dart';

class WeatherDetailsPage extends MVC_Module<WeatherDetailsModel, WeatherDetailsView, WeatherDetailsCtrl> {

  final WeatherDetailsModel model = WeatherDetailsModel();
  final WeatherDetailsView view = WeatherDetailsView();
  final WeatherDetailsCtrl ctrl = WeatherDetailsCtrl();
}