
import 'package:oui_sncf_weather/constants/config/environment.dart';
import 'package:oui_sncf_weather/models/weather.data.dart';
import 'package:oui_sncf_weather/services/cache.dart';
import 'package:oui_sncf_weather/services/http.dart';

class WeatherProvider extends HttpService {
  static final path = "/forecast";

  WeatherProvider(context) : super(context);

  CacheService get cacheService => CacheService(context).instance;

  // Should return a list of weather images data
  Future<List<WeatherDataModel>> getWeatherList(String city, bool force) async => await this.getList<WeatherDataModel>(
      path: path,
      params: {
        "appid": Environment.OpenWeatherApiKey,
        "units":"metric",
        "q": city
      },
      factory: (data) => WeatherDataModel.fromJson(data, city),
      method: HTTPMethods.GET,
      responseProperty: 'list',
      paramsAsString: true,
      force: force || cacheService.isWeatherListExpired(city),
      cache: true,
      onResponse: (response) { if(response.statusCode == 200) cacheService.lastWeatherListSync[city] = DateTime.now(); }
  );
}
