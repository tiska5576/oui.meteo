
import 'package:flutter/material.dart';
import 'package:oui_sncf_weather/components/labels/custom.label.dart';
import 'package:oui_sncf_weather/components/labels/subtitle.label.dart';
import 'package:oui_sncf_weather/components/labels/title.label.dart';
import 'package:oui_sncf_weather/components/layout/custom.container.dart';
import 'package:oui_sncf_weather/constants/config/routes.dart';
import 'package:oui_sncf_weather/constants/theme/theme.colors.dart';
import 'package:oui_sncf_weather/constants/theme/theme.sizes.dart';
import 'package:oui_sncf_weather/models/weather.data.dart';
import 'package:oui_sncf_weather/services/route.dart';
import 'package:oui_sncf_weather/services/theme.dart';
import 'package:oui_sncf_weather/services/translation.dart';
import 'package:oui_sncf_weather/tools/date.dart';
import 'package:oui_sncf_weather/tools/text.dart';

class WeatherListItem extends StatelessWidget {

  final WeatherDataModel weather;
  final String city;

  WeatherListItem(this.weather, this.city);

  @override
  Widget build(BuildContext context) {

    Map<String, String> translate = TranslationService(context).get;
    bool isDark = ThemeService(context).instance.isDark;

    return Padding(
      padding: ThemeSizes.padding[Sizes.M],
      child: CustomContainer(
        child: ListTile(
          onTap: () => RouteService(context).goTo(Routes.Details, params: {
            RouteParams.ItemId: weather.id,
            RouteParams.City: city
          }),
          leading: Hero(
            tag: 'IMAGE-${weather.id}}',
            child: CircleAvatar(
                backgroundColor: isDark ? ThemeColors.AvatarColorDark : ThemeColors.AvatarColor,
                backgroundImage: NetworkImage(weather.iconUrl)
            ),
          ),
          title: Hero(
              tag: 'TITLE-${weather.id}}',
              child: TitleLabel("${weather.temperature.toInt()}° ${DateTool.dayWord(weather.date, translate)}", size: Sizes.S)
          ),
          subtitle: Hero(
              tag: 'SUBTITLE-${weather.id}}',
              child: CustomLabel(TextTool.capitalize(translate[weather.description]), style: Theme.of(context).textTheme.overline)
          ),
          trailing: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Hero(
                  tag: 'DATE-${weather.id}}',
                  child: SubtitleLabel(DateTool.frenchDateFormat(weather.date, showYear: false), size: Sizes.M)
              ),
              CustomLabel(DateTool.toTime(weather.date, showS: false), style: Theme.of(context).textTheme.overline)
            ],
          ),
        ),
      ),
    );
  }



}